#! /usr/bin/python3

class Node:
    def __init__(self, data):
        self.left = None
        self.data = data
        self.right = None
    def insert_data(self, data):
        if not self.left:
            self.left = Node(data)
            print('setting left')
            return True
        elif not self.right:
            self.right = Node(data)
            print('setting right')
            return True
        else:
            child_l, child_r = self.get_child_count()
            if child_l <= child_r:
                self.left.insert_data(data)
                print('inserting left')
            else:
                self.right.insert_data(data)
                print('inserting right')
    def print_repr(self):
        print(self.data)
        if self.left:
            self.left.print_repr()
        if self.right:
            self.right.print_repr()
    def get_child_count(self):
        left = 0
        right = 0
        if self.left:
            count = 1
            nest_l, nest_r = self.left.get_child_count()
            left = count + nest_l + nest_r
        if self.right:
            count = 1
            nest_l, nest_r = self.right.get_child_count()
            right = count + nest_l + nest_r
        return left, right
    #def traverse(self):

root = Node(3)
root.insert_data(4)
root.insert_data(5)
root.insert_data(6)
root.insert_data(7)
root.insert_data(8)

root.print_repr()

print(root.get_child_count())

"""
setting left
setting right
setting left
inserting left
setting left
inserting right
setting right
inserting left
3
4
6
8
5
7
(3, 2)
"""
