#! /usr/bin/python3

class Node:
    def __init__(self, data):
        self.left = None
        self.data = data
        self.right = None
    def insert_data(self, data):
        if data == self.data:
            print(f'dropping number {data}, matches current data value {self.data}')
            return
        elif self.data > data:
            if not self.left:
                print(f'setting left: {data}')
                self.left = Node(data)
            else:
                print(f'adding to left child: {data}')
                self.left.insert_data(data)
        else:
            if not self.right:
                print(f'setting right: {data}')
                self.right = Node(data)
            else:
                print(f'adding to right child: {data}')
                self.right.insert_data(data)
    def print_repr(self):
        if self.left:
            self.left.print_repr()
        print(self.data)
        if self.right:
            self.right.print_repr()
    def get_child_count(self):
        left = 0
        right = 0
        if self.left:
            count = 1
            nest_l, nest_r = self.left.get_child_count()
            left = count + nest_l + nest_r
        if self.right:
            count = 1
            nest_l, nest_r = self.right.get_child_count()
            right = count + nest_l + nest_r
        return left, right
    #def traverse(self):

root = Node(10)
root.insert_data(2)
root.insert_data(4)
root.insert_data(1)
root.insert_data(6)
root.insert_data(20)
root.insert_data(11)
root.insert_data(15)

root.print_repr()

child_l, child_r = root.get_child_count()

print(f'left: {child_l}\nright: {child_r}')

"""
setting left: 2
adding to left child: 4
setting right: 4
adding to left child: 1
setting left: 1
adding to left child: 6
adding to right child: 6
setting right: 6
setting right: 20
adding to right child: 11
setting left: 11
adding to right child: 15
adding to left child: 15
setting right: 15
1
2
4
6
10
11
15
20
left: 4
right: 3
"""
